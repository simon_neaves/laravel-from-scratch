
<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


DB::listen(function($query){
    var_dump($query->sql, $query->bindings);
});

/**
 * from https://laracasts.com/series/whats-new-in-laravel-5-1/episodes/2
View::composer('stats', function($view){
    $view->with('stats', app('App\Stats'));
});
 */


Route::get('/', 'PagesController@home');
Route::get('about', 'PagesController@about');

Route::get('cards', 'CardsController@index');
Route::get('cards/{card}', 'CardsController@show');

Route::post('cards/{card}/notes', 'NotesController@store');
Route::get('notes/{note}/edit', 'NotesController@edit');
Route::patch('notes/{note}', 'NotesController@update');

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function() {
    Route::get('/', ['as' => 'dashboard', 'middleware' => 'admin', function () {
        return 'Admin Dashboard';
    }]);
});

//dd(route('admin.dashboard'));

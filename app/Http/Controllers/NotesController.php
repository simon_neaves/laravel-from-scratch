<?php

namespace App\Http\Controllers;

use App\Card;
use App\Note;
use Illuminate\Http\Request;

use App\Http\Requests;

class NotesController extends Controller
{
    public function store(Card $card)
    {
        /*
        $card->notes()->save(
            new Note(request()->all())
        );
        $card->notes()->create(
            request()->all()
        );
        */
        $this->validate(request(), [
            'body' => 'required|min:10'
        ]);

        $card->addNote(
            new Note(request()->all()),
            1
        );

        return back();
    }

    public function edit(Note $note)
    {
        return view ('notes.edit', compact('note'));
    }

    public function update(Note $note)
    {
        $note->update(request()->all());
        return back();
    }
}

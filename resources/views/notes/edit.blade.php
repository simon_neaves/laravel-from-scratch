@extends('layout')

@section('content')
    <h1>Edit Note</h1>


    <form method="POST" class="form" action="/notes/{{ $note->id }}">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        <div class="form-group">
            <textarea name="body" class="form-control">{{ $note->body }}</textarea>
        </div>
        <div class="form-group">
            <button class="btn btn-primary" type="submit">Save note</button>
        </div>
    </form>

@stop
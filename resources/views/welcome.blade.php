@extends('layout')

@section('content')
    <h1>Laravel 5</h1>
    @foreach ($people as $person)
        <li>{{$person}}</li>
    @endforeach

    @include('stats')
@stop
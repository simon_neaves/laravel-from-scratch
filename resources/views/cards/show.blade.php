@extends('layout')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h1>{{$card->title}}</h1>
            <ul class="list-group">
                @foreach ($card->notes as $note)
                    <li class="list-group-item clearfix">
                        <a class="pull-right btn btn-primary" href="/notes/{{$note->id}}/edit">Edit</a>
                        {{ $note->body }} <br>
                        - <a href="#">{{$note->user->username}}</a>
                    </li>
                @endforeach
            </ul>
            <hr>
            <h3>Add a new note</h3>

            <form method="POST" class="form" action="/cards/{{ $card->id }}/notes">
                {{ csrf_field() }}
                <div class="form-group">
                    <textarea name="body" class="form-control">{{old('body')}}</textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Add note</button>
                </div>
            </form>
            @if (count($errors))
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            @endif
        </div>

    </div>
@stop
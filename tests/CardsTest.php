gu<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Card;

class CardsTest extends TestCase
{
    use DatabaseTransactions;

    public function testCardsIndex()
    {
        $card = factory(Card::class)->create();
        $this->visit('/cards')
            ->see($card->title);
    }
}

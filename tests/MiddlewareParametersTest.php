gu<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MiddleWareParametersTest extends TestCase
{
    use DatabaseTransactions;

    public function testAuthorised()
    {
        $adminUser = factory(\App\User::class)->create(['username' => 'simon.neaves']);

        $this
            ->actingAs($adminUser)
            ->visit('/admin')
            ->see('Admin Dashboard')
            ->seePageIs('/admin')
        ;
    }

    public function testNotAuthorised()
    {
        $this->visit('/admin')
            ->seePageIs('/');
    }
}

<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Card;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        User::truncate();
        factory(User::class, 20)->create();

        Card::truncate();
        factory(Card::class, 50)->create();

    }
}
